# %% librerias
import pandas as pd

vuelos = pd.read_csv('base_datos_2008.csv', nrows = 1e5)
vuelos.head(6)

# %%
vuelos.shape
# %% 
vuelos.columns
#%%
vuelos.dtypes
#%% filter
## las columnas de mas interes parecen ser ArrDelay (retraso en la llegada)
## y DepDelay (Retraso en la partida)
vuelos['AirTime'] # tiempo de vuelo?
vuelos['Distance'] # Distancia en km?
#%%
vuelos['Dest'].unique() # Destino

#%% 
# CarrierDelay + WeatherDelay, NASDelay + SecurityDelay + LateAircraftDelay igual a DepDelay?
vuelos['DepDelay'].describe()

#%% filtros// vuelos que salen con retraso
vuelos[vuelos['DepDelay'] > 60]