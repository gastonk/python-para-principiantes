## Empleo de modulos de pytho

# buscar en internet python modules, pypi.org
"""
from datetime  import * 
# asi lo utilizaba en jupyter, de esta forma no es necesario anteponer el nombre del modulo
cada vez que se va a emplear una funcion del mismo, 
o bien si queremos una sola funcion del mismo
from datetime import timedelta
"""

import datetime

print(datetime.date.today())

print(datetime.timedelta(minutes = 100))