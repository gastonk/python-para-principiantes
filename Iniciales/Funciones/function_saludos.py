## Definicion de funciones 

# sin parametros 

def hello_world():
	print("Hello World")

hello_world()

# con parametros

def hello_tu(nombre):
	print("Hola " + nombre)

hello_tu("Gaston")

# con parámetro asignado por defecto

def hello_anywho(nombre = "quien sea"):
	print("Hola " + nombre)

hello_anywho()