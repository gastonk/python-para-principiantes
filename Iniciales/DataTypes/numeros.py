## A cerca del tipo numérico 

## Enteros
10, 100, 50

## Decimales 
a = 10.2
# print(dir(a)) revisar que se puede hacer

print(str(a))

# Operaciones

2 + 2   # suma

3*2     # producto

3**2    # potenciacion

2/2     # division

10//7   # division, solo la parte entera ()

10%7    # division, solo el residuo

