## Exploración de Strings  

cadena = "Hola Mundo" 

## dir muestra las propiedades y los métodos que soporta la variable "cadena".
## los que aparecen con __dir__ corresponden a las propiedades y los últimos a métodos.
print(dir(cadena))

## pasando el método upper y lower a cadena
print(cadena.upper())
print(cadena.lower())
print(cadena.swapcase())

### remplazar cadenas
print(cadena.replace("Mundo","Gaston"))

## Concatenar métodos
print(cadena.replace("Mundo","Gaston").lower())

## contar un elemento específico en una cadena
print(cadena.count("l"))

## Verificar que la cadena empieza especificamente con cierto/s caracter/es  
print(cadena.startswith("Hello")) ## complemento: cadena.endswith("texto")
print(cadena.startswith("Hola"))

### Dividir cadena
print(cadena.split()) ## por defecto divide por el espacio, se puede especificar el argumento con 'arg'

print(cadena.split('n'))

## Buscar la posición (indice) en la que se encuentra cierto caracter en la cadena.
print(cadena.find('H')) # equivalente cadena.index('H')

## Emplear indices
print(cadena[5])

## La longitud de la cadena es una propiedad, por ello se escribe idefectiblemente de la siguiente manera
print(len(cadena))  

## explorar clase
print(cadena.isnumeric())


### otras caracteristicas

print(cadena + " verde") # equivalente a
print(f"{cadena} verde") # solo en python superior a 3