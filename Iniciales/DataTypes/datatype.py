# Clase String

print("Hello World") # String 1
print("Hello world") # String 2

# los strings anteriores no son iguales
print("Hello World" == "Hello world") #la comparación funciona con dos iguales

print(type('Hello world'))

# Concatenación de Strings

print('By' + 'fun')  

# Clase Numbers

## Integer
print(30)
print(type(30))

## Float
print(30.001)
print(type(30.001))

# Boolean
True
print(True)
print(type(True))

# Lista
[10, 5, 20, 69]

['Hola', 'Chau', 'Nos Vemos']

['Hola', 5, 'Nos Vemos']

print(['Hola', 5, 'Nos Vemos'])
print(type(['Hola', 5, 'Nos Vemos']))

# Tuples (es una lista que no se puede cambiar o sea es una lista inmutable)
print(10,20,30)
print(type((10,20,30)))

# Diccionarios 
## un solo tipo de datos, para un mismo registro organizandose en clave y valor
## {
#   "clave1": "valor para clave 1",
#   "clave2": "valor para clave 2" 
# }
# ej. atributos para gaston
{
    "nombre":"Gaston",
    "apellido":"Torres",
    "apodo":"negro"
}

# ej. localizacion de localidad uno

{
    "lat": -61.52,
    "long": -23.74
}

### impresion de un diccionario
print({
    "nombre":"Gaston",
    "apellido":"Torres",
    "apodo":"negro"
}
)

print(type({
    "nombre":"Gaston",
    "apellido":"Torres",
    "apodo":"negro"
}
))