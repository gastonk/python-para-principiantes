## Diccionarios
## Conjunto de datos definidos por claves y valores
## Ejemplo: Carro de compras
cart = {
    "name": "book",
    "quantity": 5,
    "price": 5.2,
    "name2": "book2",
    "quantity2": 10,
    "price2": 42.3
}

print(cart)

print(type(cart), dir(cart))

## explorar claves del carro
print(cart.keys())

## explorar items del carro
print(cart.items())

### Diccionarios definidos dentro de listas

productos = [
    {"nombre":'libro1', "price": 52.3},
    {"nombre":'anotador', "price": 11.3}
]

print(productos)