demo_list =     [1, "hola", 1.35, True, [1,2,3]]
colores_base =  ['red', 'green','blue']

r = list(range(1,10)) # el rango es 10 -1 por eso retorna 9 elementos 1, 2 .., 9
print(r)
#print(type(r))

print(dir(colores_base))

print(len(colores_base)) # cantidad de elementos en la lista

print(colores_base[2])  #indices en listas

print('green' in colores_base) # averiguar si un elementos esta en una lista
print('grey' in colores_base)

print(colores_base)
colores_base[1] = 'yellow' #  reasignar elementos a una lista 
print(colores_base)

## aplicación de métodos a listas

colores_base.append('white')                ## agregar elemento a la lista
colores_base.append(['black', 'orange'])    ## append soporta un solo argumento, para agregar mas de un elemento a la lista una posibilidad es emplear otra lista
print(colores_base)

## agregar mas de un elemento a la lista sin crear tuplas o listas dentro de la misma
colores_base =  ['red', 'green','blue']
colores_base.extend(['black', 'orange']) 
print(colores_base)

### agregar elementos en posiciones determinadas  
colores_base =  ['red', 'green','blue']
colores_base.insert(0,'white')
print(colores_base)

## Quitar último elemento de una lista
colores_base.pop()
print(colores_base) 

# quitar elemento especifico de la lista
colores_base.remove('red')
print(colores_base)

# quitar elemento por indice 
colores_base.pop(1)
print(colores_base) 

## limpiar lista
colores_base.clear()
print(colores_base)

#@@#3 reiniciando colores
colores_base =  ['red', 'green','blue']
colores_base.extend(['black', 'orange']) 
print(colores_base)

colores_base.sort() # los ordena alfabeticamente
print(colores_base)

colores_base.sort(reverse=True) # a la invers del anterior
print(colores_base)

## averiguar donde se ubica un elemento en la lista
print('red' in colores_base)     #primero verificar que se encuentre
print(colores_base.index('red'))        # Luego dónde esta

colores_base.append('black')
print(colores_base.count('black'))