## Como guardar variables

nombre  =  "Gastón"

print(nombre)

# Case sensitive
book = "El árbol de las brujas"
Book = "El árbol de las brujas"

print(
    book, 
    Book
    )
# book es igual a Book, pero no son el mismo ojeto
print(book == Book)

# #no válido
# 2book
# # válido
# _2book
# book2

## Guardar multiples variables en la misma linea
x, book2 = 100, "Algún día volveré"

print(x)

print(book2)

## Convenciones
book_name = "nombre del libro" #Snake Case
bookName = "nombre del libro" #Camel case
BookName = "nombre del libro" #Pascal Case

# Constantes en mayúscula

PI = 3.14161