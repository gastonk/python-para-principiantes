## Tuples
# Son listas cuyos elementos no se modifican

x = (1,2,3)

print(type(x))

print(dir(x))

### al ser una clase de lista especial, soporta menos metodos

print(x[2])

del x; #elimina x
