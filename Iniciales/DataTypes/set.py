# SET
## El tipo set es un subtipo de lista desordenada, o sea, no tiene indice

nombres = {'Cesar','Gaston','Camilo'}

print(type(nombres))

print(dir(nombres))  

print('Camila' in nombres) ## explora si Camila esta en el set (igual que en listas)

#como no esta lo agrego
nombres.add('Camila')
print(nombres)

nombres.add('Adriana')
print(nombres)

### al correr varias veces, observar que el orden de la última impresión cambia 

nombres.remove('Cesar')
print(nombres)