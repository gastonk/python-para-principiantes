## Condicionales múltiples por disyunción o conjunción

# operador de conjunción lógica and
# operador de disyunción lógica or

nombre = input("Ingrese su nombre:  ")
apellido = input("Ahora su apellido: ")

if nombre == "John" and apellido == "Carter":
    print("Hi, John Carter!")
elif nombre == "John" and apellido != "Carter":
    print("John, tu no eres Carter!")
else:
    print("John no vino")
