# Primer for

foods = ['apples', 'bread', 'cheese', 'milk', 'bananas', 'graves']

""" print("PRIMER FOR")
for comida in foods:
     print(comida)  

## agregando un condicional
print("For + IF")

for comida in foods:
    if comida == 'milk':
        print("Debes comprar leche")
    print(comida) 
 """
print("ADD BREAK IN LOOP (for)")

for comida in foods:
    if comida == 'milk':
        print("Debes comprar leche")
        break
    print(comida) 

print("ADD CONTINUE IN LOOP FOR")

for comida in foods:
    if comida == 'milk':
        continue
    print(comida)
