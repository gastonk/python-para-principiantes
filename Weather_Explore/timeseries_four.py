# el objetivo es aplicar resample en time serie object
#%% modules
import os
import matplotlib.pyplot as plt
import seaborn as sns
import pandas as pd

from pandas.plotting import register_matplotlib_converters
register_matplotlib_converters()

sns.set(font_scale = .8, style = 'whitegrid')

#%% load data
path_file = os.path.join(os.getcwd(), 'earth-analytics',
                                      'data',
                                      'colorado-flood',
                                      'precipitation',
                                      '805325-precip-daily-2003-2013.csv')

precip_horaria_2003_2013 = pd.read_csv(path_file,
                                    parse_dates=['DATE'],
                                    index_col=['DATE'],
                                    na_values=[999.99])
precip_horaria_2003_2013.describe()
# %%
f, ax = plt.subplots(figsize = (10, 4))

ax.scatter(precip_horaria_2003_2013.index.values,
          precip_horaria_2003_2013['HPCP'],
          color = 'darkgreen')

ax.set(xlabel = 'Fecha',
       ylabel = 'Precipitacion (pulgadas)',
       title = 'Precipitacion horaria - Estacion de Boulder\n2003-2013')
# %%
precip_diaria_2003_2013 = precip_horaria_2003_2013.resample('D').sum()

f, ax = plt.subplots(figsize = (10, 4))

ax.scatter(precip_diaria_2003_2013.index.values,
           precip_diaria_2003_2013['HPCP'],
           color = 'darkblue')

ax.set(xlabel = 'Fecha',
       ylabel = 'Precipitacion (pulgadas)',
       title = 'Precipitacion Diaria - Estacion de Boulder\n2003-2013')
# %%
precip_mensual_2003_2013 = precip_horaria_2003_2013.resample('M').sum()

f, ax = plt.subplots(figsize = (10, 4))

ax.scatter(precip_mensual_2003_2013.index.values,
           precip_mensual_2003_2013['HPCP'],
           color = 'darkblue')

ax.set(xlabel = 'Fecha',
       ylabel = 'Precipitacion (pulgadas)',
       title = 'Precipitacion Mensual - Estacion de Boulder\n2003-2013')
# %%
precip_anual_2003_2013 = precip_diaria_2003_2013.resample('Y').sum()

f, ax = plt.subplots(figsize = (10, 4))

ax.scatter(precip_anual_2003_2013.index.values,
           precip_anual_2003_2013['HPCP'],
           color = 'orange')

ax.set(xlabel = 'Fecha',
       ylabel = 'Precipitacion (pulgadas)',
       title = 'Precipitacion Anual en Boulder (Colorado - EEUU)\n2003-2013')
