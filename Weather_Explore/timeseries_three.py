#%% modules + seteos
import os
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns

from pandas.plotting import register_matplotlib_converters
register_matplotlib_converters()

sns.set(font_scale = .8, style='whitegrid')

#%%
file_path = os.path.join(os.getcwd(), 'earth-analytics',
                                      'data',
                                      "colorado-flood",
                                      "precipitation",
                                      "805325-precip-dailysum-2003-2013.csv")
# %%
data = pd.read_csv(file_path,
                    parse_dates=['DATE'],
                    index_col=['DATE'],
                    na_values=[999.99])
# %%
data.head()
#%%
data.info()
#%%
data.describe()

#%% subset by year
data['2005'].describe()
#%% subset by mes
data[data.index.month == 5].describe()

#%% subset by intervalo

data['2011-10-01':'2011-12-31'].head()
data['2011-10-01':'2011-12-31'].tail()

# %%
print(data.index.min())
#%%
print(data.index.max())

#%% plot a interval
precip_agos_octubre_2013 = data['2013-08-01':'2013-10-31']

f, ax = plt.subplots(figsize = (12, 6))

ax.bar(precip_agos_octubre_2013.index.values,
       precip_agos_octubre_2013['DAILY_PRECIP'],
       color = 'darkblue')

ax.set(xlabel = 'Fecha',
       ylabel = 'Precipitacion (pulgadas)',
       title = 'Precipitacion Diaria\nAgosto-Octubre 2013 en Boulder Creek')

# Rotate tick marks on x-axis
plt.setp(ax.get_xticklabels(), rotation=45)

plt.show()
# %%
