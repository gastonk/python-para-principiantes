#%% packages
from matplotlib.axes._axes import _log as matplotlib_axes_logger
import os
import matplotlib.pyplot as plt
import seaborn as sns
import pandas as pd
import earthpy as et
# %% conversiones de date time entre pandas y matplotlib
from pandas.plotting import register_matplotlib_converters
register_matplotlib_converters()

sns.set(font_scale=0.8, style="whitegrid")

# Dealing with error thrown by one of the plots
matplotlib_axes_logger.setLevel('ERROR')
import warnings
warnings.filterwarnings('ignore')


# %% download data prebuilding
data = et.data.get_data('colorado-flood')

#%% luego redireccionar a este directorio
print(os.getcwd())

#%%
os.chdir(os.path.join(et.io.HOME, 'earth-analytics', "data"))

# Define relative path to the data
file_path = os.path.join("colorado-flood",
                         "precipitation",
                         "805325-precip-daily-2003-2013.csv")

# Import the file as a pandas dataframe
boulder_precip_2003_2013 = pd.read_csv(file_path)
boulder_precip_2003_2013.head()

# %%
print(os.getcwd())

#%%
boulder_precip_2003_2013.plot(
    x = 'DATE',
    y = 'HPCP',
    title = 'Precipitacion Diaria'
)
plt.show()
# %%
boulder_precip_2003_2013['HPCP'].describe()
# hay que indicar que 999.99 es NA

#%% revision de formatos
boulder_precip_2003_2013.dtypes
#%%
# Importacion de los datos usando datatime y especificando valor de NAs 
boulder_precip_2003_2013 = pd.read_csv(file_path,
                                       # Asegurar que DATE tenga formato de datetime
                                       parse_dates=['DATE'],
                                       # Set DATE como el index index
                                       index_col=['DATE'],
                                       # especificar como se asignan NAs
                                       na_values=['999.99'])

# revisar datos
boulder_precip_2003_2013.head()
#%% 
boulder_precip_2003_2013['HPCP'].describe()
# %%
boulder_precip_2003_2013.dtypes
#%%
boulder_precip_2003_2013.describe()
#%%
boulder_precip_2003_2013.index
#%% 
boulder_precip_2003_2013.plot(y = "HPCP", title = "Precipitacion por hora")
#%% subset by index
precipt_2005 = boulder_precip_2003_2013.loc['2005']
precipt_2005.head()

#%% clean
precipt_2005 = precipt_2005.dropna()
precipt_2005.reset_index().plot(y = 'HPCP',
                  x= 'DATE',
                  title = 'Ppt por hora en 2005',
                  kind = 'scatter')
#%% mismo graf con matplotlib
f, ax = plt.subplots()
ax.scatter(x = precipt_2005.index.values,
           y = precipt_2005['HPCP'])
plt.show()

#%% sumatoria por dia
ppt_diaria_2005 = precipt_2005.resample('D').sum()
#%%
f, ax = plt.subplots()
ax.scatter(x = ppt_diaria_2005.index.values,
           y = ppt_diaria_2005['HPCP'])

#%%
precip_2012 = boulder_precip_2003_2013.loc['2012'].dropna()
precip_2013 = boulder_precip_2003_2013.loc['2013'].dropna()
#%%
fig,(ax1, ax2) = plt.subplots(2, 1, sharex=False, clear = False, figsize=(10, 10))
ax1.scatter(x = precip_2012.index.values,
            y = precip_2012['HPCP'])
ax1.title.set_text('Precipitacion horaria en 2012')
ax1.set(ylim = (0,2))

ax2.scatter(x = precip_2013.index.values,
            y = precip_2013['HPCP'])
ax2.title.set_text('Precipitacion horaria en 2013')
plt.show()
# %% promedio de precipitacion horaria 2012 vs 2013
[precip_2012['HPCP'].mean().round(2), precip_2013['HPCP'].mean().round(2)]
# %% maxima precipitacion horaria 2012 vs 2013
[precip_2012['HPCP'].max(), precip_2013['HPCP'].max()]

#%% ver precipitaciones de primavera en 213
f, ax = plt.subplots()
ax.scatter(x = precip_2013['2013-09-01':'2013-11-01'].index.values,
           y = precip_2013['2013-09-01':'2013-11-01']['HPCP'])
ax.title.set_text('Precipitaciones Sept-Nov 2013')
ax.set(ylim = [0,2.3])
#%%
precip_2013['2013-09-01':'2013-11-01']['HPCP'].max()

#%% extra
from matplotlib.dates import DateFormatter

# Place your code to plot your data here
flood_data = boulder_precip_2003_2013['2013-09-01':'2013-11-01']

f, ax = plt.subplots(figsize=(10, 6))

ax.scatter(x=flood_data.index.values,
           y=flood_data["HPCP"])

# Define the date format
date_form = DateFormatter("%m-%d")
ax.xaxis.set_major_formatter(date_form)
ax.set(title="Optional Challenge \n Precipitation Sept - Nov 2013 \n Optional Plot with Dates Formatted Cleanly")
plt.show()
# %%
