#%% seteos
import os
import matplotlib.pyplot as plt
from pandas.io.parsers import read_csv
import seaborn as sns
import pandas as pd
import earthpy as et

from pandas.plotting import register_matplotlib_converters
register_matplotlib_converters()

sns.set(font_scale = .8, style='whitegrid')
# %% download data
# file_url = 'https://ndownloader.figshare.com/files/12948515'
# et.data.get_data(url = file_url)

# definir directorio de trabajo
os.chdir(os.path.join(et.io.HOME, 'earth-analytics'))

# ruta relativa al archivo
file_path = os.path.join('data', 'earthpy-downloads',
                          'july-2018-temperature-precip.csv')

# importacion del archivo
boulder_july_2018 = pd.read_csv(file_path)
# %%
boulder_july_2018.head()
# %%
boulder_july_2018.info()

#%% plot inicial
fig, ax = plt.subplots(figsize = (10, 10))

# ejes
ax.plot(boulder_july_2018['date'],
        boulder_july_2018['precip'],
        color='purple')

# titulos
ax.set(xlabel = 'Fecha',
       ylabel= 'Precipitacion (pulgadas)',
       title = 'Precipitacion Diaria total \nBoulder, Colorado (Julio de 2018)')
# %%
boulder_july_2018['precip'].describe()
# %%
# Todo lo anterior muestra que la columna date se cargo como un string, que los 
# datos presentan valores perdidos imputados como -999.000
# a continuación se cargan los datos como corresponde

boulder_july_2018 = pd.read_csv(file_path,
                                parse_dates=['date'],
                                index_col=['date'],
                                na_values=-999)
# %%
f, ax = plt.subplots(figsize = (10,5))
ax.scatter(boulder_july_2018.index.values,
           boulder_july_2018['precip'],
           color = 'darkred')

ax.set(xlabel= 'Fecha',
        ylabel = 'Precipitacion (pulgadas)',
        title = 'Precipitacion total diaria\nBoulder, Colorado (Julio 2018)')
# %%
boulder_july_2018['precip'].resample('M').sum()*2.54*10