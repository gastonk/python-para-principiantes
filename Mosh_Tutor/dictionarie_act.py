costumer = {
    "name": "Daiana Leiva",
    "age": 33,
    "is_verified": False
}

costumer["name"] = "Daiana Sainz"
print(costumer.get("birthdate"))  # nacimiento no definido
print(costumer.get("birthdate", '1 de octubre de 1988'))  # asignado por defecto

costumer["birthdate"] = "Octubre 1 1988"        # Definiendo nuevos campos
print(costumer)
