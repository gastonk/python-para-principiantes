# Aleatorizaciones

import random

print("Tres Aleateorizacion entre 0-1")
for i in range(3):
    print(random.random())

print("Tres Aleateorizacion de enteros")
for i in range(3):
    print(random.randint(10, 50))

print("Selecion aleatoria a partir de una lista")
gente = ["carla", "Diana", "Gaston", "L", "Pedro"]
lider = random.choice(gente)
print(lider)

print("Tupla aleatoria")
point = (random.randint(0,9),random.randint(0,9))
print(point)

print("Jugando a los dados")
class Dado:
    def roll(self):
        primero = random.randint(1,6)
        segundo = random.randint(1,6)
        return (primero, segundo)

intento1 = Dado()

print(intento1.roll())