# Uso de while

orden = ""
started = False
stoped = True

while True:
    orden = input('>').lower()
    if orden == "help":
        print('''
start - to start the car
stop - to stop the car
quit - to exit
''')
    elif orden == "start":
        if started:
            print('Car is already started')
        else:
            started = True
            stoped = False
            print('Car started .. Ready to go!!')
    elif orden == "stop":
        if stoped:
            print('Car is already stopped!.')
        else:
            stoped = True
            started = False
            print('Car stopped.')
    elif orden == "quit":
        break
    else:
        print("I don't understand that ...")
