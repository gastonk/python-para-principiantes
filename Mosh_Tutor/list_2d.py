matrix = [
    [1, 2, 3],
    [4, 5, 6],
    [7, 8, 9]
]

print(type(matrix))

print(matrix[0])

print(matrix[0][0])

for row in matrix:
    for item in row:
        print(item)