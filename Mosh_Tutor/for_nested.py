# Nested for loops

# Este loop arma un arreglo de las combinaciones de x, y
# serviría cuando se tienen dos factores y se precisa armar un arreglo con dichas convinaciones (ortografía te la debo)
# la dejo comentada para que funcione el segundo ejercicio

# for x in range(4):
#    for y in range(3):
#        print(f'({x}, {y})')

numbers = [5, 2, 5, 2, 2, 2]

for number in numbers:
    salida = ''
    for l in range(number):
        salida += '*'
    print(salida)
