# Con clases se definen nuevos tipos de objetos para modelar conceptos reales
# A continuacion se define la clase 'Point' que soporta methodos especificos
# Por convencion las clases se nombran con mayusculas la primera letra: NombreCompuesto

class Point:
    def __init__(self, x, y):
        self.x = x
        self.y = y
    def move(self):
        print('move')
    def draw(self):
        print('draw')

# point1 = Point(1000, 20)
# print(point1.x)
# point1.draw()

class Person:
    def __init__(self, name):
        self.name = name
    
    def talk(self):
        print(f"Hi, I am {self.name}")

person1 = Person("Jhon Smith")

person1.talk()
