import colorama
from colorama import Fore, Style, Back
colorama.init(autoreset = True)

name = input("Ingresa tu nombre: ")

if len(name) < 3:
    print(Fore.RED + 'ERROR!' + Fore.RESET + ' el nombre debe tener al menos tres caracteres.')
elif len(name) > 50:
    print(Fore.RED + 'ERROR!' + Fore.RESET + ' el máximo de caracteres es de 50.')
else:
    print(Fore.GREEN + 'Su nombre luce bien!')


#print(Fore.RED + Back.GREEN + "red text on green back")
