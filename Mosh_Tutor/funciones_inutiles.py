emojis = {
    ":)": "😃",
    ":(": "😣",
    ";)": "😜"
}


def emojis_translate():
    mensaje = input('>:')
    mensaje = mensaje.split(" ")
    output = ''
    for word in mensaje:
        output += emojis.get(word, word) + " "
    print(output)


emojis_translate()
