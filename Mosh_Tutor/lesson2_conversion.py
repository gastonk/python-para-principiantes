# Example mosh
# nacimiento = input('Año de nacimiento: ')
# nacimiento = int(nacimiento)
# age = 2021 - nacimiento
# print(age)

# Exercise 1
"""
weight = input("Cuál es tu peso en libras?: ")
weight = float(weight)*0.4535
print("Tu peso en kilogramos es ", weight)
"""
# print(weight)

# Code Improve. 2 inputs and 1 else
try:
    weight = int(input("Cual es tu peso: "))
    unidad = input("(L)bs) o (K)g: ")
    if unidad.upper() == 'L':
        conversion = weight * 0.45
        print(f"Tu peso es {conversion} en Kilos.")
    else:
        conversion = weight / 0.45
        print(f'Tu peso es {conversion} en Libras')
except ValueError:
    print('Tipo de dato no soportado')

