# Definicion de Strings
course = "Python's Course for Geginners"   # Dobles comillas para permitir apostrofe
print(course)

course = 'Python for "Beginners"'  # Comillas simples permite incorporar comillas dobles dentro del string
print(course)

# Strings de multiples lineas
mail = ''' 
Hola Juana, 

Este es nuestro primer e-mail.

Gracias!!

Equipo de soporte.
'''
print(mail)
# print(len(mail))

first = 'Juana'
last = 'Molina'

# imprimir Juana [Molina] is a coder
print(f"{first} [{last}] is a coder")

print(last.upper())