mensaje = input(">")
words = mensaje.split(' ')

emojis = {
    ":)": "😃",
    ":(": "😣",
    ";)": "😜"
}

output = ""
for word in words:
    output += emojis.get(word, word) + " "
print(output)
