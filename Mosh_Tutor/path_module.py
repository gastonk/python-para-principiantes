# Como trabajar con rutas y directorios
from pathlib import Path

# Rutas relativas
path_rel = Path('Mosh_Tutor')
print(path_rel.exists())

# Rutas absolutas
path_a = Path("/usr/lib/python3.9/")
print(path_a.exists())

# Listar elementos de un directorio 
print(path_a.glob('*'))

# Generator object es un topico avanzado, espero llegarle
# para imprimir la lista se recurre a un for

# Todos lo elementos en el directorio
#for ob in path_a.glob('*'):
#    print(ob)

# Solamente los archivos
# for ob in path_a.glob('*.*'):
#    print(ob)

# Solamente los archivos de python
for ob in path_a.glob("*.py"):
    print(ob)