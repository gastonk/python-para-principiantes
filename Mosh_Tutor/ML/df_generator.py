# %%
from pathlib import Path
import pandas as pd


# %%
ruta = Path("/home/gt/Documentos/Biometria/PythonForBeginners/Mosh_Tutor/ML")
print(ruta.exists())

# %%
data_eg = [
    {'age':20, 'gender':1, 'music':'HipHop'},
    {'age':23, 'gender':1, 'music':'HipHop'},
    {'age':25, 'gender':1, 'music':'HipHop'},
    {'age':26, 'gender':1, 'music':'Jazz'},
    {'age':29, 'gender':1, 'music':'Jazz'},
    {'age':30, 'gender':1, 'music':'Jazz'},
    {'age':31, 'gender':1, 'music':'Classical'},
    {'age':33, 'gender':1, 'music':'Classical'},
    {'age':37, 'gender':1, 'music':'Classical'},
    {'age':20, 'gender':0, 'music':'Dance'},
    {'age':21, 'gender':0, 'music':'Dance'},
    {'age':25, 'gender':0, 'music':'Dance'},
    {'age':26, 'gender':0, 'music':'Rock'},
    {'age':27, 'gender':0, 'music':'Rock'},
    {'age':30, 'gender':0, 'music':'Rock'},
    {'age':31, 'gender':0, 'music':'Classical'},
    {'age':34, 'gender':0, 'music':'Classical'},
    {'age':35, 'gender':0, 'music':'Classical'}
]

data_eg = pd.DataFrame(data_eg)
data_eg

# %%
data_eg.to_csv(r'datos.csv', index = False)

# %% temporal para borrar archivos inecesarios
from os import remove
# remove("datos")
# remove("ruta")
