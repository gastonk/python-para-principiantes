# %% importacion de modulos + lectura de datos
import pandas as pd
from sklearn.tree import DecisionTreeClassifier
from sklearn.model_selection import train_test_split
from sklearn.metrics import accuracy_score

music_df = pd.read_csv('datos.csv')
music_df.head()
music_df.columns

# %% preparacion de datos
X = music_df.drop(columns = ['music'])
Y = music_df['music']
X_train, X_test, Y_train, Y_test = train_test_split(X, Y, test_size= .2)

# %%  prediccion
model =  DecisionTreeClassifier()
model.fit(X_train, Y_train)
predictions = model.predict(X_test)

#  %% exploracion de las predicciones
score = accuracy_score(Y_test, predictions)
score

# %% persistencia
import joblib
joblib.dump(model, 'music_predictor.joblib')

# %% save graph of model
from sklearn import tree
tree.export_graphviz(model, out_file = 'music_predictor.dot',
                    feature_names = ['edad','sexo'],
                    class_names = sorted(Y_train.unique()),
                    label = 'all',
                    rounded = True,
                    filled = True
                    )
