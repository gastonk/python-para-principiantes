# Python for Beginners  

Códigos desarrollados durante la capacitación.

* **

## Iniciales  

En /iniciales se guardan los archivos basados en el curzo de Fazt.

### Tipos de Datos  

* **helloworld.py**  popular programa inicial que pinta por pantalla *Hello, World!*
* **datatype.py** permite explorar los principales tipos de datos en @python.
* **variables.py** acerca de como se almacenan variables y cuales son las principales convenciones empleadas.  
* **string.py** exploración del tipo de dato *string*.
* **numeros.py** acerca de los números y operaciones básicas en python.
* **listas.py** definición de listas, atributos y operaciones más comunes.
* **tuples.py**  definición de una tupla y métodos que soporta.  
* **set.py** definición de un set, algunos métodos, etc
* **dictionaries.py** definición de diccionarios, algunos métodos, etc.  

* **

### Condicionales  

Uso básico de condicionales en archivos:  

* **menor_a.py** condicional básico con operador <.  

* **colores.py** condicional *if* ampliado con elif (else if abreviado).  

* **john.py** condicional con operadores lógicos múltiples.  

* **

### Bucles  

Implementación base de procesos iterativos, especificando *break* y *continue*:  

* **for_uno.py**: definición de for  mediante elementos de un objeto.

* **for_range.py** igual que el previo pero definiendo un valor rango.  

* **while.py**  

* **

### Funciones  

Definición de funciones sencillas  

* **function_saludos.py** funciones sin o con parametros, predefinidos o sin predefinir.  

* **function_suma.py**: suma dos enteros.  

* **

### Modulos  

Definir modulos (*my_math.py*) y emplear modulos propios, de terceros y de python.  

Extras

* **nacimiento.py** calcula el año de nacimiento en base a la edad brindada en años.  

* **

## Mosh

Dos objetivos:  

1. Generar una página web e

2. implementar un proceso de aprendizaje automatizado.

* classes.py: acerca de como emplear [Class] para definir un objeto
